﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration; //Для возможности использования app.config
using System.Data.SqlClient;
using NLog;
using NLog.Internal;
using PlcMsgTransfer;

namespace MyProject
{
    public partial class Form1 : Form
    {
        private static Logger loggerMsgManager = LogManager.GetCurrentClassLogger();
        private static string[] Data;
        private DictionaryObject Objects;
        private DictionaryMessage Messages;
        private static string strTable = "";

        // Nlog
        static void SendLogFunc(string Msg)
        {
            loggerMsgManager.Debug(Msg);
        }
        public Form1()
        {
            SendLogFunc("Инициализация приложения");
            InitializeComponent();
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            /*Объявляем строковую переменную и записываем в нее
             строку подключения 
             Data Source - имя сервера, по стандарту (local)\SQLEXPRESS
             Initial Catalog - имя БД 
             Integrated Security=-параметры безопасности
                                @"Provider=SQLOLEDB;
                                Data Source=FILATOV\SQLSERVER;
                                Initial Catalog=SEM_MPSA_HISTORY;
                                Connect Timeout=5;
                                Integrated Security=SSPI";
             */
            string connectionString = "Provider=" + System.Configuration.ConfigurationManager.AppSettings.Get("Provider") +
                             ";Data Source=" + System.Configuration.ConfigurationManager.AppSettings.Get("Data Source") +
                             ";Initial Catalog=" + System.Configuration.ConfigurationManager.AppSettings.Get("Initial Catalog") +
                             ";Connect Timeout=" + System.Configuration.ConfigurationManager.AppSettings.Get("Connect Timeout") +
                             ";Integrated Security=" + System.Configuration.ConfigurationManager.AppSettings.Get("Integrated Security");

            /* Создаем экземпляр класса  OleDbConnection по имени conn
            и передаем конструктору этого класса, строку подключения
            */
            OleDbConnection conn = new OleDbConnection(connectionString);

            // пробуем подключится
            try
            {
                conn.Open();
            }
            catch
            {
                try
                {
                    MessageBox.Show("Повторная попытка подключения к бд");
                    conn.Open();
                }
                catch (Exception se)
                {
                    MessageBox.Show("Невозможно подключиться к бд");
                    SendLogFunc(se.Message);
                    Application.Exit();
                    return;
                }
            }


            //Проверяем структуру таблицы 

            OleDbCommand Checkdb = new OleDbCommand("SELECT * FROM PLCMessage" , conn);
            OleDbDataReader dr = Checkdb.ExecuteReader();

            Data = new string[dr.FieldCount - 1];
            for (int i = 1; i < dr.FieldCount; i++)
                Data[i - 1] = dr.GetName(i).ToString().Trim();
            
            strTable = System.Configuration.ConfigurationManager.AppSettings.Get("structTable");
                try 
                {
                    for (int i = 0; i < Data.Length; i++)
                    strTable.Contains(Data[i]);
                }
                catch
                {
                    MessageBox.Show("Ошибка в структуре таблицы");
                    SendLogFunc("Ошибка в структуре таблицы");
                }


            // Проверяем наличие таблицы PLCMessage 
            try
            {
                string NameTable = "SELECT Top 1 * FROM PLCMessage";

                // посылаем запрос comand к БД ...результат в dr
                OleDbCommand AvailableTable = new OleDbCommand(NameTable, conn);
                OleDbDataReader dr1 = AvailableTable.ExecuteReader();

                dr1.Close();

                MessageBox.Show("Таблица PLCMessage существует");
            }
            catch
            {
                // Если таблицы нет, то создаем ее
                MessageBox.Show("PLCMessage НЕ cуществует, создаем ее");
                OleDbCommand cmdCreateTable = new OleDbCommand("CREATE TABLE " +
                                            "PLCMessage (ID decimal(10,0) not null" +
                                            ", IDmess decimal(10,0) not null" +
                                            ", UserName nvarchar(150) not null" +
                                            ", Place nvarchar(150) not null" +
                                            ", DTime datetime not null" +
                                            ", DTimeAck nvarchar(150) not null" +
                                            ", SysID decimal(3,0) not null" +
                                            ", SysNum decimal(3,0) not null" +
                                            ", Mess decimal(3,0) not null" +
                                            ", Message nvarchar(300) not null" +
                                            ", isAck decimal(1,0) not null" +
                                            ", Priority decimal(1,0) not null," +
                                            "Value nvarchar(150) not null)", conn);
                // Посылаем запрос
                try
                {
                    cmdCreateTable.ExecuteNonQuery();
                    MessageBox.Show("Таблица создана успешно");
                }
                catch
                {
                    MessageBox.Show("Ошибка при создании таблицы");
                    SendLogFunc("Ошибка при создании таблицы");
                }
            }
            
            // Проверяем размер таблицы
            OleDbCommand sizeTable = new OleDbCommand("sp_spaceused", conn);
            sizeTable.CommandType = CommandType.StoredProcedure;
            sizeTable.Parameters.AddWithValue("@objname", "PLCMessage");
            OleDbDataReader reader = sizeTable.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    textBox1.Text = reader["Data"].ToString();
                }
            }
            reader.Close();
            conn.Close();
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }


        private string WriteMessToDB(int iStart, int iStop, byte[] Butt, string DateTime, Int32 IDmess, Dictionary<string, string> objects)
        {
            byte[] _Butt = Butt;
            byte SysID;                                                                             //Идентификатор объекта
            byte Mess;                                                                              //Код сообщения    
            Int16 SysNum;                                                                           //Номер объекта
            float Val;                                                                              //Значение
                                                                                                    //string DT = Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond);
                                                                                                    //MessageBox.Show(Convert.ToString(SqlDb.connection.State));
                                                                                                    //MessageBox.Show(Convert.ToString(SqlDb.connection.ConnectionString));

            string connectionString = "Provider=" + System.Configuration.ConfigurationManager.AppSettings.Get("Provider") +
                          ";Data Source=" + System.Configuration.ConfigurationManager.AppSettings.Get("Data Source") +
                          ";Initial Catalog=" + System.Configuration.ConfigurationManager.AppSettings.Get("Initial Catalog") +
                          ";Connect Timeout=" + System.Configuration.ConfigurationManager.AppSettings.Get("Connect Timeout") +
                          ";Integrated Security=" + System.Configuration.ConfigurationManager.AppSettings.Get("Integrated Security");
            OleDbConnection conn = new OleDbConnection(connectionString);

            for (int jj = iStart; jj <= iStop; jj++)
            {

                SysID = _Butt[10 + (jj - 1) * 8];

                if (SysID == 255)
                {
                    DateTime = "";


                    #region преобразование времени новое

                    if (_Butt[12 + (jj - 1) * 8] < 10)//год
                    {
                        DateTime = DateTime + "200" + Convert.ToString(_Butt[12 + (jj - 1) * 8]) + "-";
                    }
                    else
                    {
                        DateTime = DateTime + "20" + Convert.ToString(_Butt[12 + (jj - 1) * 8]) + "-";
                    }
                    if (_Butt[13 + (jj - 1) * 8] < 10)//месяц
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[13 + (jj - 1) * 8]) + "-";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[13 + (jj - 1) * 8]) + "-";
                    }
                    if (_Butt[14 + (jj - 1) * 8] < 10)//день
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[14 + (jj - 1) * 8]) + " ";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[14 + (jj - 1) * 8]) + " ";
                    }

                    if (_Butt[15 + (jj - 1) * 8] < 10)//час
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[15 + (jj - 1) * 8]) + ":";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[15 + (jj - 1) * 8]) + ":";
                    }

                    if (_Butt[16 + (jj - 1) * 8] < 10)//мин
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[16 + (jj - 1) * 8]) + ":";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[16 + (jj - 1) * 8]) + ":";
                    }

                    if (_Butt[17 + (jj - 1) * 8] < 10)//сек
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[17 + (jj - 1) * 8]) + ".000";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[17 + (jj - 1) * 8]) + ".000";
                    }
                    #endregion

                }
                else
                {
                    if (DateTime != "Null" && DateTime != "")
                    {
                        try
                        {

                            Mess = _Butt[11 + (jj - 1) * 8];
                            //Array.Reverse(_Butt, 12 + (jj - 1) * 8, sizeof(Int16));
                            SysNum = BitConverter.ToInt16(_Butt, 12 + (jj - 1) * 8);
                            //Array.Reverse(_Butt, 14 + (jj - 1) * 8, sizeof(Single));
                            Val = BitConverter.ToSingle(_Butt, 14 + (jj - 1) * 8);
                            // преобразование времени
                            //SqlDb.connection.Open();
                            OleDbCommand comm = new OleDbCommand("INSERT INTO PLCMessage (IDmess, Place, DTime, DTimeAck, SysID, SysNum, Mess, Message, isAck, Priority, Value) VALUES (?,?,?,?,?,?,?,?,?,?,?)", conn);
                            comm.Parameters.Add("IDmess", OleDbType.Decimal).Value = IDmess;
                            comm.Parameters.Add("Place", OleDbType.VarChar).Value = "PLC";
                            comm.Parameters.Add("DTime", OleDbType.DBTimeStamp).Value = DateTime;


                            if (SysID < 10)
                            {
                                if (Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck == 1)
                                {
                                    comm.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = Global.STRNOTACK;
                                }
                                else
                                {
                                    comm.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = "";
                                }
                            }
                            else if ((SysID >= 10) && (SysID < 100))
                            {
                                if (Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck == 1)
                                {
                                    comm.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = Global.STRNOTACK;
                                }
                                else
                                {
                                    comm.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = "";
                                }
                            }
                            else
                            {
                                if (Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).isAck == 1)
                                {
                                    comm.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = Global.STRNOTACK;
                                }
                                else
                                {
                                    comm.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = "";
                                }
                            }
                            comm.Parameters.Add("SysID", OleDbType.Integer).Value = SysID;
                            comm.Parameters.Add("SysNum", OleDbType.Integer).Value = SysNum;
                            comm.Parameters.Add("Mess", OleDbType.Integer).Value = Mess;

                            if (SysID < 10)
                            {
                                comm.Parameters.Add("Message", OleDbType.VarChar).Value = Objects.getObject("00" + Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Mess;
                                comm.Parameters.Add("isAck", OleDbType.Integer).Value = Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck;
                                comm.Parameters.Add("Priority", OleDbType.Integer).Value = Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Priority;
                                //MessageBox.Show(Convert.ToString(Objects.getObject("00" + Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Mess));
                                // MessageBox.Show(Convert.ToString(Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck));
                                // MessageBox.Show(Convert.ToString(Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Priority));
                            }
                            else if ((SysID >= 10) && (SysID < 100))
                            {
                                comm.Parameters.Add("Message", OleDbType.VarChar).Value = Objects.getObject("0" + Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).Mess;
                                comm.Parameters.Add("isAck", OleDbType.Integer).Value = Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck;
                                comm.Parameters.Add("Priority", OleDbType.Integer).Value = Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).Priority;
                            }
                            else
                            {
                                comm.Parameters.Add("Message", OleDbType.VarChar).Value = Objects.getObject(Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).Mess;
                                comm.Parameters.Add("isAck", OleDbType.Integer).Value = Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).isAck;
                                comm.Parameters.Add("Priority", OleDbType.Integer).Value = Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).Priority;
                            }
                            comm.Parameters.Add("Value", OleDbType.VarChar).Value = Val;

                            comm.ExecuteNonQuery();

                        }
                        catch (Exception e)
                        {
                            SendLogFunc(e.Message);
                        }
                    }
                }
            }
            //MessageBox.Show(DT + "-" + Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond));
            return DateTime;


        }

    }
}








