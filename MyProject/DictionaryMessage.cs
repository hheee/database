﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using NLog.Internal;
using System.Data.OleDb;

namespace PlcMsgTransfer
{
    class DictionaryMessage
    {
        private Dictionary<string, SMessage> Messages;

        public DictionaryMessage()
        {
            Messages = new Dictionary<string, SMessage>();
            ReadMessageFromDB();
        }

        public SMessage getMessage(string MessageKey)
        {
            if (Messages.ContainsKey(MessageKey))
            {
                return Messages[MessageKey];
            }
            else
            {
                return new SMessage(-1, 1, MessageKey, 1, 1, 1, 1, 1, 1, 1);
            }
        }

        public void ReadMessageFromDB()
        {
            Messages.Clear();
            //string connectionString = "Provider=sqloledb;Data Source=localhost\\sqlexpress;Password=zaqew;User ID=orpo;Initial Catalog=SEM_MPSA;Connect Timeout=2";
            string connectionString = "Provider=" + System.Configuration.ConfigurationManager.AppSettings.Get("Provider") +
                                      ";Data Source=" + System.Configuration.ConfigurationManager.AppSettings.Get("Data Source") +
                                      ";Initial Catalog=" + System.Configuration.ConfigurationManager.AppSettings.Get("Initial Catalog") +
                                      ";Connect Timeout=" + System.Configuration.ConfigurationManager.AppSettings.Get("Connect Timeout") +
                                      ";Integrated Security=" + System.Configuration.ConfigurationManager.AppSettings.Get("Integrated Security");
            OleDbConnection conn = new OleDbConnection(connectionString);
            try
            {
                conn.Open();
                OleDbCommand comm1 = new OleDbCommand("Select * from Message", conn);
                OleDbDataReader reader = comm1.ExecuteReader();
                while (reader.Read())
                {
                    if (Convert.ToInt32(reader["SysID"]) < 10)
                    {
                        Messages["00" + Convert.ToString(reader["SysID"]) + Convert.ToString(reader["Mess"])] = new SMessage(Convert.ToInt32(reader["SysID"]),
                                                                Convert.ToInt32(reader["Mess"]), Convert.ToString(reader["Message"]), Convert.ToInt32(reader["Kind"]),
                                                                Convert.ToInt32(reader["Priority"]), Convert.ToInt32(reader["Sound"]), Convert.ToInt32(reader["IDSound"]),
                                                                Convert.ToInt32(reader["Type"]), Convert.ToInt32(reader["isAck"]), Convert.ToInt32(reader["IDColor"]));
                    }
                    else if ((Convert.ToInt32(reader["SysID"]) >= 10) && (Convert.ToInt32(reader["SysID"]) < 100))
                    {
                        Messages["0" + Convert.ToString(reader["SysID"]) + Convert.ToString(reader["Mess"])] = new SMessage(Convert.ToInt32(reader["SysID"]),
                                                                Convert.ToInt32(reader["Mess"]), Convert.ToString(reader["Message"]), Convert.ToInt32(reader["Kind"]),
                                                                Convert.ToInt32(reader["Priority"]), Convert.ToInt32(reader["Sound"]), Convert.ToInt32(reader["IDSound"]),
                                                                Convert.ToInt32(reader["Type"]), Convert.ToInt32(reader["isAck"]), Convert.ToInt32(reader["IDColor"]));
                    }
                    else
                    {
                        Messages[Convert.ToString(reader["SysID"]) + Convert.ToString(reader["Mess"])] = new SMessage(Convert.ToInt32(reader["SysID"]),
                                                                Convert.ToInt32(reader["Mess"]), Convert.ToString(reader["Message"]), Convert.ToInt32(reader["Kind"]),
                                                                Convert.ToInt32(reader["Priority"]), Convert.ToInt32(reader["Sound"]), Convert.ToInt32(reader["IDSound"]),
                                                                Convert.ToInt32(reader["Type"]), Convert.ToInt32(reader["isAck"]), Convert.ToInt32(reader["IDColor"]));
                    }
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception)
            {
            }
        }
    }
}
