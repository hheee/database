﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlcMsgTransfer
{
    class SObject
    {
        private int _SysID;
        private int _SysNum;
        private string _Name;
        private int _Sound1;
        private int _Sound2;
        private int _Sound3;

        public SObject()
        {
        }

        public SObject(int setSysID, int setSysNum, string setName, int setSound1, int setSound2, int setSound3)
        {
            _SysID = setSysID;
            _SysNum = setSysNum;
            _Name = setName;
            _Sound1 = setSound1;
            _Sound2 = setSound2;
            _Sound3 = setSound3;
        }

        public int SysID
        {
            set { _SysID = value; }
            get { return _SysID; }
        }

        public int SysNum
        {
            set { _SysNum = value; }
            get { return _SysNum; }
        }

        public string Name
        {
            set { _Name = value; }
            get { return _Name; }
        }

        public int Sound1
        {
            set { _Sound1 = value; }
            get { return _Sound1; }
        }

        public int Sound2
        {
            set { _Sound2 = value; }
            get { return _Sound2; }
        }

        public int Sound3
        {
            set { _Sound3 = value; }
            get { return _Sound3; }
        }
    }
}
