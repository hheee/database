﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using NLog.Internal;
using System.Data.OleDb;

namespace PlcMsgTransfer
{
    class DictionaryObject
    {
        private Dictionary<string, SObject> Objects;

        public DictionaryObject()
        {
            Objects = new Dictionary<string, SObject>();
            ReadObjectsFromDB();
        }

        public SObject getObject(string Objectkey)
        {
            if (Objects.ContainsKey(Objectkey))
            {
                return Objects[Objectkey];
            }
            else
            {
                return new SObject(-1, 1, Objectkey, -1, -1, -1);
            }

        }

        public void ReadObjectsFromDB()
        {
            Objects.Clear();
            string connectionString = "Provider=" + System.Configuration.ConfigurationManager.AppSettings.Get("Provider") +
                                      ";Data Source=" + System.Configuration.ConfigurationManager.AppSettings.Get("Data Source") +
                                      ";Initial Catalog=" + System.Configuration.ConfigurationManager.AppSettings.Get("Initial Catalog") +
                                      ";Connect Timeout=" + System.Configuration.ConfigurationManager.AppSettings.Get("Connect Timeout") +
                                      ";Integrated Security=" + System.Configuration.ConfigurationManager.AppSettings.Get("Integrated Security");
            OleDbConnection conn = new OleDbConnection(connectionString);
            try
            {
                conn.Open();
                OleDbCommand comm1 = new OleDbCommand("Select * from Object", conn);
                OleDbDataReader reader = comm1.ExecuteReader();
                while (reader.Read())
                {
                    if (Convert.ToInt32(reader["SysID"]) < 10)
                    {
                        Objects["00" + Convert.ToString(reader["SysID"]) + Convert.ToString(reader["SysNum"])] = new SObject(Convert.ToInt32(reader["SysID"]),
                                                                Convert.ToInt32(reader["SysNum"]), Convert.ToString(reader["Name"]), Convert.ToInt32(reader["Sound1"]),
                                                                Convert.ToInt32(reader["Sound2"]), Convert.ToInt32(reader["Sound3"]));
                    }
                    else if ((Convert.ToInt32(reader["SysID"]) >= 10) && (Convert.ToInt32(reader["SysID"]) < 100))
                    {
                        Objects["0" + Convert.ToString(reader["SysID"]) + Convert.ToString(reader["SysNum"])] = new SObject(Convert.ToInt32(reader["SysID"]),
                                                                Convert.ToInt32(reader["SysNum"]), Convert.ToString(reader["Name"]), Convert.ToInt32(reader["Sound1"]),
                                                                Convert.ToInt32(reader["Sound2"]), Convert.ToInt32(reader["Sound3"]));
                    }
                    else
                    {
                        Objects[Convert.ToString(reader["SysID"]) + Convert.ToString(reader["SysNum"])] = new SObject(Convert.ToInt32(reader["SysID"]),
                                                                Convert.ToInt32(reader["SysNum"]), Convert.ToString(reader["Name"]), Convert.ToInt32(reader["Sound1"]),
                                                                Convert.ToInt32(reader["Sound2"]), Convert.ToInt32(reader["Sound3"]));
                    }
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception)
            {
            }

        }
    }
}
